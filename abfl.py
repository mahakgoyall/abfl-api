import pyodbc
import os,json
import logging
import configparser
import datetime
from threading import Thread
import requests

logging.basicConfig(filename='abfl.log',level=logging.DEBUG, format='%(levelname)s:%(asctime)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

# read config file
def readConfig():
    baseDirectory = os.path.dirname(os.path.abspath(__file__))
    config = configparser.ConfigParser()
    config.read(os.path.join(baseDirectory+ '/config.ini'))
    return config


# function for creating a database connection
def create_connection():
    try:
        config = readConfig()
        driver = config['SQL_Credentials']['driver']
        server = config['SQL_Credentials']['server']
        database = config['SQL_Credentials']['database_name']
        uid = config['SQL_Credentials']['uid']
        password = config['SQL_Credentials']['password']
        connection = pyodbc.connect(
            "driver={};server={};database={};uid={};PWD={}".format(driver, server, database, uid, password),
            autocommit=True)
    except Exception as e:
        logging.error("Exception in establishing database connection."+str(e))
        return 0
    return connection


def get_token(proxydict):
    config = readConfig()
    accessToken = 0
    try:
        token_api = str(config['api_details']['token_api'])
        headers = {'Content-Type': 'application/json'}
        token_payload = {'username': config['token_headers']['username'],
                         'password': config['token_headers']['password'],
                         'InstitutionId': config['token_headers']['InstitutionId'],
                         'application': config['token_headers']['application']}
        tokenResponse = requests.post(url=token_api, data=json.dumps(token_payload), headers=headers, proxies=proxydict)
        tokenResponse = tokenResponse.content.decode('utf-8')
        tokenJson = json.loads(tokenResponse)
        accessToken = tokenJson['oBody']['payLoad']['OauthResponse']['oauthToken']['access_token']
    except Exception as e:
        logging.error("Exception in fetching access token from api: "+str(e))
    return accessToken


def main():
    config = readConfig()
    proxydict = {'https': "https://" + str(config['proxy_settings']['proxy_url']) + ":" + str(config['proxy_settings']['proxy_port'])}
    accessToken = get_token(proxydict)
    headerdict = {'sReqType': config['api_headers']['sReqType'],
                  'sAppSource': config['api_headers']['sAppSource'],
                  'sSourceID': config['api_headers']['sSourceID'],
                  'sAppID': config['api_headers']['sAppID'],
                  'sProduct': config['api_headers']['sProduct'],
                  'dtSubmit': config['api_headers']['dtSubmit'],
                  'sCroId': config['api_headers']['sCroId'],
                  'sDsaId': config['api_headers']['sDsaId'],
                  'sInstID': config['api_headers']['sInstID'],
                  'sBranchCode': config['api_headers']['sBranchCode'],
                  'sLoginId': config['api_headers']['sLoginId'],
                  'sLoginRole': config['api_headers']['sLoginRole'],
                  'instituteName': config['api_headers']['instituteName'],
                  'sDealerId': config['api_headers']['sDealerId']}
    conn = create_connection()
    cur = conn.cursor()
    sqlResponse = []
    try:
        sqlResponse = cur.execute("exec SP_Get_Bank_Branch_Updated").fetchall()
    except Exception as e:
        print("exception in fetching data from stored procedure: ",e)
        logging.error("exception in fetching data from stored procedure: "+str(e))
    column_name = ()
    for column in sqlResponse:
        column_name = column
        break
    print("sqlResponse:  ", sqlResponse)

    def Thread_func(dataset):
        conn = create_connection()
        cur = conn.cursor()
        try:
            logging.debug("running for records: "+str(dataset))
            datadict = dict()
            for item, col in zip(dataset, column_name):
                col = "s" + str(col)
                datadict[col] = item
            payloadDict = {'oHeader': headerdict,
                           'oDataToUpdate': dict(oBankDetailMaster=datadict, bUpdate=True)}
            print("payslod dict.........", payloadDict)
        except Exception as e:
            print("Error in building data: "+str(e))
        try:
            url = config['api_details']['ifsc_api']
            headers = {'Authorisation': f'Bearer {accessToken}', 'Content-Type': 'application/json', 'institutionID':'4032'}
            apiResponse = requests.post(url=url, data=json.dumps(payloadDict), headers=headers, proxies=proxydict)
            print(apiResponse)
            apiResponse = apiResponse.content.decode('utf-8')
            print(apiResponse, "api response")
        except Exception as e:
            print("Error in sending data to apsi: "+str(e))
        try:
            apiJson = json.loads(apiResponse)
            statusCode = apiJson['oStatus']['iStatus']
            if statusCode == 200:
                logging.debug("data sent successfully with output: "+str(apiJson))
                querystring = "exec sp_update_insert_failed_task " + str(dataset)
                querystring = querystring.replace("None", "''").replace("(","")
                querystring = querystring[:-1] + ", 1"
                logging.debug("querystring: "+ str(querystring))
                cur.execute(querystring)
            else:
                querystring = "exec sp_update_insert_failed_task " + str(dataset)
                querystring = querystring.replace("None", "''").replace("(","")
                querystring = querystring[:-1] + ", 0"
                logging.debug("querystring: "+ str(querystring))
                cur.execute(querystring)
        except Exception as e:
            logging.error("Exception in calling the api: "+str(e))
            querystring = "exec sp_update_insert_failed_task " + str(dataset)
            querystring = querystring.replace("None", "''").replace("(","")
            querystring = querystring[:-1] + ", 0"
            logging.debug("querystring: "+ str(querystring))
            cur.execute(querystring)
        conn.close()

    thread_list = list()
    for idx, record in enumerate(sqlResponse):
        try:
            if idx == 0:
                continue
            thread_list.append(Thread(target=Thread_func, args=(record,)))
            thread_list[-1].start()
            if len(thread_list) > 100:
                for i in thread_list:
                    i.join()
                thread_list = list()
        except Exception as e:
            print("exception in threading:"+str(e))
            logging.error(str(record) + " Exception in threading: " + str(e))
    conn.close()


if __name__ == "__main__":
    main()
