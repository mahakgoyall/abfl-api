import os,json, logging, configparser, datetime, requests
from os import access
from threading import Thread
import pyodbc

logging.basicConfig(filename='abfl_logger.log',level=logging.DEBUG, format='%(levelname)s:%(asctime)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

def readConfig():
    try:
        baseDirectory = os.path.dirname(os.path.abspath(__file__))
        config = configparser.ConfigParser()
        config.read(os.path.join(baseDirectory+ '/config.ini'))
        return config
    except Exception as e:
        logging.error("Error in reading configuration file: "+str(e))
        return 0

def get_token():
    config = readConfig()
    if config == 0:
        return 0
    accessToken = 0
    try:
        token_api = str(config['base_url']['token_base_url']) + str(config['api_url']['token_url'])
        headers = {'Content-Type': 'application/json'}
        token_payload = {'username': config['token_headers']['username'],
                         'password': config['token_headers']['password'],
                         'InstitutionId': config['token_headers']['InstitutionId'],
                         'application': config['token_headers']['application']}
        # Token API Call
        tokenResponse = requests.post(url=token_api, data=json.dumps(token_payload), headers=headers)
        tokenResponse = tokenResponse.content.decode('utf-8')
        tokenJson = json.loads(tokenResponse)
        accessToken = tokenJson['oBody']['payLoad']['OauthResponse']['oauthToken']['access_token']
        logging.info("tokenJson: " + str(accessToken))
    except Exception as e:
        logging.error("Exception in fetching access token from api: "+str(e))
        return 0
    return accessToken

def create_connection():
    try:
        config = readConfig()
        if config == 0: return 0
        driver = config['SQL_Credentials']['driver']
        server = config['SQL_Credentials']['server']
        database = config['SQL_Credentials']['database_name']
        uid = config['SQL_Credentials']['uid']
        password = config['SQL_Credentials']['password']
        # Connection String
        connection = pyodbc.connect("driver={};server={};database={};uid={};PWD={}".format(driver, server, database, uid, password),autocommit=True)
    except Exception as e:
        logging.error("Exception in establishing database connection."+str(e))
        return 0
    return connection
