from logging import exception
import os, json
import logging
import queue
import pyodbc
from threading import Thread
import threading
import requests
from operator import itemgetter
from token_generation import (get_token,readConfig,create_connection)

logging.basicConfig(filename="abfl_logger.log",level=logging.DEBUG,
    format="%(levelname)s:%(asctime)s:%(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p")

def get_master_by_id(payload, resp_uuid):
    try:
        config = readConfig()
        logging.info("Entered Get Master by ID method for generic master")
        url = (
            str(config["base_url"]["token_base_url"])
            + str(config["api_url"]["get_generic_master_search"])
            + str(config["add_master_record_api"]["InstitutionId"])
            + str("/" + payload["sMasterId"])
            + str(config["api_url"]["get_generic_master_params"])
            + str(resp_uuid)
        )
        headers = {
            "Authorisation": f"Bearer {get_token()}",
            "institutionID": config["add_master_record_api"]["InstitutionId"]}
        data_payload = {}
        apiResponse = requests.get(url=url, headers=headers, data=data_payload)
        apiResponse_content = apiResponse.content.decode("utf-8")
        apiJson = json.loads(apiResponse_content)
        record_id = ""
        for d in apiJson["oBody"]["payLoad"]["data"]:
            if "uuid" in d:
                if d["uuid"] == resp_uuid:
                    logging.info(
                        "record id found against uuid is :" + str(d["sRecordId"]))
                    record_id = d["sRecordId"]
        return record_id
    except Exception as e:
        logging.error("Exception occured in get master by id : " + str(e))

def save_api_response(apiJson, payload, cur, resp_uuid):
    try:
        logging.info("Entered Save API Response Method!")
        master_id = payload["sMasterId"]
        master_name = ""
        lst = payload["aMasterRecords"][0].keys()
        record_id = ""
        if payload["sMasterType"] == "DROPDOWN":
            payload_ddl = str(
                json.dumps(apiJson["oBody"]["payLoad"]["aDropDownValueDetails"]))
            for ele in lst:
                if (
                    json.loads(payload_ddl).pop()[ele]
                    == payload["aMasterRecords"][0][ele]
                ):
                    record_id = json.loads(payload_ddl).pop()["sRecordId"]
                else:
                    break
        else:
            record_id = get_master_by_id(payload, resp_uuid)
        record_desc = str(json.dumps(payload["aMasterRecords"]))
        record_type = "ADD Master Record"
        failed_reason = str(apiJson["oStatus"])
        sync_status = "0" if apiJson["oStatus"]["iStatus"] == 200 else "1"
        params = (master_id, master_name, record_id, record_desc, record_type,failed_reason,sync_status)
        logging.info("Final parameter formed for proc is : " + str(params))
        sql = """EXEC SaveAPIResponseFailedRecord @MasterID=?, @MasterName=?, @RecordId=?,
         @RecordDesc=?, @Request_Type=?, @Failed_Reason=?,@SyncStatus=?"""
        cur.execute(sql, params)

    except Exception as e:
        logging.error(
            "Exception occured in Save API Response while calling PROC : " + str(e))

def add_master_record_api(payload, conn, cur):
    config = readConfig()
    logging.info("Entered add master record api method")
    try:
        url = str(config["base_url"]["token_base_url"]) + str(
            config["api_url"]["add_master_record"])
        headers = {
            "Authorisation": f"Bearer {get_token()}",
            "Content-Type": "application/json",
            "institutionID": config["add_master_record_api"]["InstitutionId"]}
        apiResponse = requests.post(
            url=url, data=json.dumps(payload), headers=headers)
        apiResponse_content = apiResponse.content.decode("utf-8")
        apiJson = json.loads(apiResponse_content)
        logging.info(
            "Add API response status code is :" + str(apiJson["oStatus"]["iStatus"]))
        resp_uuid = (
            ""
            if payload["sMasterType"] == "DROPDOWN"
            else apiJson["oBody"]["payLoad"]["uuid"])
        save_api_response(apiJson, payload, cur, resp_uuid)
    except Exception as e:
        logging.error("Exception in calling ADD MASTER api: " + str(e))

def create_payload(record, config, cur):
    try:
        masterRecord = dict()
        payload = ""
        masterRecordData = [cur.execute("EXEC LENTRA_API_ADD_RECORD @MasterID=?",record[3]).fetchall()]
        tableheader = [tableHeader[0] for tableHeader in cur.description]
        for record_row in masterRecordData:
            for value in record_row:
                idx = 0
                for element in value:
                    masterRecord[tableheader[idx]] = element
                    idx = idx + 1
                payload = {
                    "sUserId": record[0],
                    "sClientId": config["add_master_record_api"]["sClientId"],
                    "sMasterType": record[2],
                    "sMasterId": record[3],
                    "aMasterRecords": [masterRecord],
                }
                logging.info(
                    "payload generated for add record api request: " + str(payload))
                yield payload
    except Exception as e:
        logging.error("Exception in creating the payload :" + str(e))

def main():
    try:
        conn = create_connection()
        logging.info("Entered main method!")
        if conn == 0:
            logging.info("conn is 0 !")
            return 0
        cur = conn.cursor()
        sqlResponse = []
        sql = "EXEC getMasterToSync @type=?"
        sqlResponse = cur.execute(
            sql,
            "INSERT",
        ).fetchall()
        logging.info("proc is executed")
        for record in sqlResponse:
            try:
                payloadList = create_payload(record, readConfig(), cur)
                for payload in payloadList:
                    if len(payload) > 0:
                        t1 = threading.Thread(target=add_master_record_api, args=(payload, conn, cur))
                        t1.start()
                        t1.join()
            except Exception as e:
                logging.error("Exception in fetching data from db: " + str(e))
        cur.close()
        conn.close()
    except Exception as e:
        logging.error("Exception in main method: " + str(e))

if __name__ == "__main__":
    main()
