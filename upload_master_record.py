from token_generation import create_connection, get_token, readConfig
import logging
import pyodbc
import json, os
import threading
from datetime import date
import requests

logging.basicConfig(filename="abfl_logger.log", level=logging.DEBUG,
        format="%(levelname)s:%(asctime)s:%(message)s",datefmt="%m/%d/%Y %I:%M:%S %p")

def save_api_response(apiJson, payload, file_name, cur):
    try:
        logging.info("Entered Save API Response Method!")    
        if(payload.__contains__('masterName')):
            master_id = payload["masterName"]
        else:
             master_id = payload["masterId"]
        master_name = ""
        record_id = ""
        record_desc = str(json.dumps([dict(apiJson["oBody"]["payLoad"])]))  
        record_type = "UPLOAD Master Record"
        failed_reason = str(apiJson["oStatus"])
        sync_status = "0" if apiJson["oStatus"]["iStatus"] == 200 or 202 else "1"
        params = (master_id,master_name,record_id,record_desc,record_type,failed_reason,sync_status)
        logging.info("Final parameter formed for proc is : " + str(params))
        sql = """EXEC SaveAPIResponseFailedRecord @MasterID=?, @MasterName=?, @RecordId=?, 
        @RecordDesc=?, @Request_Type=?, @Failed_Reason=?,@SyncStatus=?"""
        cur.execute(sql, params)
    except Exception as e:
        logging.error("Exception occured in Save API Response while calling PROC : " + str(e))

def upload_master_record_api(payload, conn, cur):
    try:
        config = readConfig()
        logging.debug("Entered UPLOAD master record api method")
        if(payload['mastertype'] == 'DROPDOWN'):
            url = str(config["base_url"]["token_base_url"]) + str(config["api_url"]["upload_master_record"])
        else:
            url = str(config["base_url"]["token_base_url"]) + str(config["api_url"]["upload_master_record_generic"])
        payload.pop('mastertype')
        relative_path = os.path.join(str(os.curdir + config["file_upload"]["upload_folder"]))
        file_paths = [relative_path + f
            for f in os.listdir(str(os.curdir + config["file_upload"]["upload_folder"]))
            if os.path.isfile(relative_path + f)]
        logging.info("total files found in file-upload directory : " + str(len(file_paths)))
        for file_path in reversed(file_paths):
            try:
                logging.info("processing file name : "+ str(file_path)+ " from file-upload directory.")
                if payload["FileName"] in file_path:
                    files = [("file", open(file_path, "rb"))]
                    headers = {
                        "InstitutionID": config["add_master_record_api"]["InstitutionId"],
                        "Authorisation": f"Bearer {get_token()}"}
                    apiResponse = requests.post(url=url, headers=headers, data=payload, files=files )
                    apiResponse_content = apiResponse.content.decode("utf-8")
                    apiJson = json.loads(apiResponse_content)
                    logging.info("UPLOAD api response status code is :"+ str(apiJson["oStatus"]["iStatus"]))
                    save_api_response(apiJson, payload, file_path, cur)
            except Exception as e:
                logging.info("exception in looping for file path from file upload directory "+ str(e))
    except Exception as e:
        logging.error("Some Exception encountered in UPLOAD Master API: " + str(e))

def create_payload(record, config, cur):
    try:
        logging.debug("Entered UPLOAD master record api method")
        payload = ""
        masterRecordData = [cur.execute("EXEC LENTRA_API_UPLOAD_RECORD @MasterID=?",record[3]).fetchall()]
        for record_row in masterRecordData:
            for value in record_row:
                if(value[2] == 'DROPDOWN'):
                    payload = {"institutionId": value[1],"masterId": value[3],"userId": value[0],
                        "type": value[4],"emailId": value[5],"FileName": value[6]}             
                else:
                    payload = {"institutionId": value[1],"masterName": value[3],"userId": value[0],
                        "type": value[4],"emailId": value[5],"FileName": value[6]}                    
                logging.info("payload generated for add record api request: " + str(payload))
                payload['mastertype'] = value[2]
                yield payload
    except Exception as e:
        logging.error("Exception in creating the payload :" + str(e))

def main():
    try:
        conn = create_connection()
        logging.info("Entered main method!")
        if conn == 0: return 0
        cur = conn.cursor()
        sqlResponse = []
        sql = "EXEC getMasterToSync @type=?"
        sqlResponse = cur.execute(sql,"UPLOAD",).fetchall()
        for record in sqlResponse:
            try:
                payloadList = create_payload(record, readConfig(), cur)
                for payload in payloadList:
                    try:
                        if len(payload) > 0:
                            t1 = threading.Thread(
                                target=upload_master_record_api, args=(payload, conn, cur))
                            t1.start()
                            t1.join()
                    except Exception as e:
                         logging.error("Exception in calling upload master api method: " + str(e))
            except Exception as e:
                logging.error("Exception in create payload method in sqlresponse: " + str(e))           
        cur.close()
        conn.close()
    except Exception as e:
        logging.error("Exception in main method: " + str(e))

if __name__ == "__main__":
    main()
