import io
import pandas as pd
import base64
from pandas import ExcelWriter
toread = io.BytesIO()
  # pass your `decrypted` string as the argument here


with open('filestream.txt','rb') as d:
    data=d.read()
#print(data)
decrypted=base64.b64decode(data)
toread.write(decrypted)
toread.seek(0)  # reset the pointer

df = pd.read_excel(toread)  # now read to dataframe

writer = ExcelWriter('DSA_CODE.xlsx')
df.to_excel(writer,'Sheet5')
writer.save()

