import os, json, logging, datetime, requests
from threading import Thread
import threading
from token_generation import create_connection, get_token, readConfig

logging.basicConfig(
    filename="abfl_logger.log",
    level=logging.DEBUG,
    format="%(levelname)s:%(asctime)s:%(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
)

def save_api_response(apiJson, payload, cur, recordId):
    try:
        logging.info("Entered Save API Response Method!")
        master_id = payload["sMasterId"]
        master_name = ""
        record_id = recordId
        record_desc = str(payload)
        record_type = "DELETE Master Record"
        failed_reason = str(apiJson["oStatus"])
        sync_status = "0" if apiJson["oStatus"]["iStatus"] == 200 else "1"
        params = (
            master_id,
            master_name,
            record_id,
            record_desc,
            record_type,
            failed_reason,
            sync_status,
        )
        logging.info("Final parameter formed for proc is : " + str(params))
        sql = """EXEC SaveAPIResponseFailedRecord @MasterID=?, @MasterName=?, @RecordId=?,
         @RecordDesc=?, @Request_Type=?, @Failed_Reason=?,@SyncStatus=?"""
        cur.execute(sql, params)
    except Exception as e:
        logging.error(
            "Exception occured in Save API Response while calling PROC : " + str(e)
        )

def create_payload(record, config):
    try:
        payload = {
            "sUserId": record[0],
            "sClientId": config["add_master_record_api"]["sClientId"],
            "sMasterId": record[2],
            "sMasterType": record[3],
        }
        logging.info("payload generated for delete request: " + str(payload))
        return payload, record[4]
    except Exception as e:
        logging.error("Exception in creating the payload" + str(e))
        return 0

def delete_api(record, cur):
    config = readConfig()
    logging.info("calling create payload method")
    payload, recordId = create_payload(record, config)
    if len(payload) == 0:
        return 0
    logging.info("Entered add master record api method")
    try:
        api_url = (
            str(config["base_url"]["api_base_url"])
            + str(config["api_url"]["delete_master_record"])
            + str(recordId)
        )
        headers = {
            "Authorisation": f"Bearer {get_token()}",
            "Content-Type": "application/json",
            "institutionID": config["add_master_record_api"]["InstitutionId"],
        }
        apiResponse = requests.delete(
            url=api_url, data=json.dumps(payload), headers=headers
        )
        apiResponse_content = apiResponse.content.decode("utf-8")
        apiJson = json.loads(apiResponse_content)
        logging.info(
            "Delete API response status code is :" + str(apiJson["oStatus"]["iStatus"])
        )
        save_api_response(apiJson, payload, cur, recordId)
    except Exception as e:
        logging.error("Error in calling delete API: " + str(e))

def main():
    try:
        conn = create_connection()
        logging.info("Entered main method!")
        if conn == 0:
            return 0
        cur = conn.cursor()
        sqlResponse = []
        sqlResponse = cur.execute("EXEC LENTRA_API_DELETE_RECORD").fetchall()
        for record in sqlResponse:
            try:
                t1 = threading.Thread(target=delete_api, args=(record, cur))
                t1.start()
                t1.join()
            except Exception as e:
                logging.error("Exception in fetching data from db: " + str(e))
        cur.close()
        conn.close()
    except Exception as e:
        logging.error("Exception in main method: " + str(e))

if __name__ == "__main__":
    main()