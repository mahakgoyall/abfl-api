import configparser
from token_generation import create_connection, get_token, readConfig
import logging
import pyodbc
import json, os
import threading
import requests

logging.basicConfig(
    filename="abfl_logger.log",
    level=logging.DEBUG,
    format="%(levelname)s:%(asctime)s:%(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
)

def get_master_by_id(payload, resp_uuid):
    try:
        config = readConfig()
        logging.info("Entered Get Master by ID method for generic master")
        url = (
            str(config["base_url"]["token_base_url"])
            + str(config["api_url"]["get_generic_master_search"])
            + str(config["add_master_record_api"]["InstitutionId"])
            + str("/" + payload["sMasterId"])
            + str(config["api_url"]["get_generic_master_params"])
            + str(resp_uuid)
        )
        headers = {
            "Authorisation": f"Bearer {get_token()}",
            "institutionID": config["add_master_record_api"]["InstitutionId"],
        }
        data_payload = {}
        proxydict = {
            "https": "https://"
            + str(config["proxy_settings"]["proxy_url"])
            + ":"
            + str(config["proxy_settings"]["proxy_port"])
        }
        apiResponse = requests.get(
            url=url, headers=headers, data=data_payload, proxies=proxydict
        )
        apiResponse_content = apiResponse.content.decode("utf-8")
        apiJson = json.loads(apiResponse_content)
        record_id = ""
        for d in apiJson["oBody"]["payLoad"]["data"]:
            if "uuid" in d:
                if d["uuid"] == resp_uuid:
                    logging.info(
                        "record id found against uuid is :" + str(d["sRecordId"])
                    )
                    record_id = d["sRecordId"]
        return record_id
    except Exception as e:
        logging.error("Exception occured in get master by id : " + str(e))

def save_api_response(apiJson, payload, cur, recordId):
    try:
        logging.info("Entered Save API Response Method!")
        master_id = payload["sMasterId"]
        master_name = ""
        record_id = (
            recordId
            if payload["sMasterType"] == "DROPDOWN"
            else get_master_by_id(payload, recordId)
        )
        record_desc = str(json.dumps(payload["aMasterRecords"]))
        record_type = "UPDATE Master Record"
        failed_reason = str(apiJson["oStatus"])
        sync_status = "0" if apiJson["oStatus"]["iStatus"] == 200 else "1"
        params = (
            master_id,
            master_name,
            record_id,
            record_desc,
            record_type,
            failed_reason,
            sync_status,
        )
        logging.info("Final parameter formed for proc is : " + str(params))
        sql = """EXEC SaveAPIResponseFailedRecord @MasterID=?, @MasterName=?, @RecordId=?, 
        @RecordDesc=?, @Request_Type=?, @Failed_Reason=?,@SyncStatus=?"""
        cur.execute(sql, params)
    except Exception as e:
        logging.error(
            "Exception occured in Save API Response while calling PROC : " + str(e)
        )

def update_master_record_api(payload, recordid, cur):
    config = readConfig()
    try:
        logging.debug("Entered update master record api method")
        url = str(config["base_url"]["token_base_url"]) + str(
            config["api_url"]["update_master_record"] + str(recordid)
        )
        headers = {
            "Authorisation": f"Bearer {get_token()}",
            "Content-Type": "application/json",
            "institutionID": config["add_master_record_api"]["InstitutionId"],
        }
        proxydict = {
            "https": "https://"
            + str(config["proxy_settings"]["proxy_url"])
            + ":"
            + str(config["proxy_settings"]["proxy_port"])
        }
        apiResponse = requests.put(
            url=url, data=json.dumps(payload), headers=headers, proxies=proxydict
        )
        # apiResponse = requests.put(url=url, data=json.dumps(payload), headers=headers)
        apiResponse_content = apiResponse.content.decode("utf-8")
        apiJson = json.loads(apiResponse_content)
        logging.info(
            "Update api response status code is :" + str(apiJson["oStatus"]["iStatus"])
        )
        resp_uuid = (
            recordid
            if payload["sMasterType"] == "DROPDOWN"
            else apiJson["oBody"]["payLoad"]["uuid"]
        )
        save_api_response(apiJson, payload, cur, resp_uuid)
    except Exception as e:
        logging.error("Some Exception encountered in UPDATE Master API: " + str(e))

def create_payload(record, config, cur):
    try:
        logging.info("calling create payload method")
        masterRecord = dict()
        record_id = ""
        masterRecordData = [
            cur.execute(
                "EXEC LENTRA_API_UPDATE_RECORD @MasterID=?", record[3]
            ).fetchall()
        ]
        tableheader = [tableHeader[0] for tableHeader in cur.description]
        for record_row in masterRecordData:
            for value in record_row:
                idx = 0
                for element in value:
                    masterRecord[tableheader[idx]] = element
                    idx = idx + 1
                record_id = value[0]
                payload = {
                    "sUserId": record[0],
                    "sClientId": config["add_master_record_api"]["sClientId"],
                    "sMasterType": record[2],
                    "sMasterId": record[3],
                    "aMasterRecords": [masterRecord],
                }
                logging.info(
                    "payload generated for UPDATE record API request: "
                    + str(payload)
                    + "record id generated is : "
                    + str(record_id)
                )
                yield payload, record_id
    except Exception as e:
        logging.error("Exception in creating the payload :" + str(e))

def main():
    try:
        conn = create_connection()
        logging.info("Entered main method!")
        if conn == 0:
            return 0
        cur = conn.cursor()
        sqlResponse = []
        sql = "EXEC getMasterToSync @type=?"
        sqlResponse = cur.execute(
            sql,
            "UPDATE",
        ).fetchall()
        for record in sqlResponse:
            try:
                result = create_payload(record, readConfig(), cur)
                for payload, record_id in result:
                    if len(payload) == 0:
                        return 0
                    t1 = threading.Thread(
                        target=update_master_record_api,
                        args=(payload, record_id, cur),
                    )
                    t1.start()
                    t1.join()
            except Exception as e:
                logging.error("Exception in fetching data from db: " + str(e))
        cur.close()
        conn.close()
    except Exception as e:
        logging.error("Exception in main method: " + str(e))

if __name__ == "__main__":
    main()
